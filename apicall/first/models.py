from django.db import models

class emp_data(models.Model):
	emp_id=models.CharField(max_length=5);
	employee_name=models.CharField(max_length=100);
	employee_salary=models.CharField(max_length=9);
	employee_age=models.CharField(max_length=3);
	profile_image=models.CharField(max_length=200);
	
	def __str__(self):
		return self.emp_id + ", " + self.employee_name + ", " + self.employee_salary + ", "+ self.employee_age;

# Create your models here.
