
1. Clone the repo
2. Install python 3.6.6, install all required libraries
3. Install django 2.2.1
4. make all migrations using the two commands `python manage.py makemigrations` and `python manage.py migrate`
5. Run teh server using `python manage.py runserver` and check if there are no errors
6. You are good to go

### First

This is the first part of the project. you can view all the employees list using `127.0.0.1:8000/first`

You will create a database using this `first` app, and import the employee list if the database is empty. then it will print the data.

### First/*id*

This is the second part of the project. you can view a particular employees  using `*127.0.0.1:8000/first/emp_id*`

### Second/?id=_&employee_name=_&employee_age=_&employee_salary=_&profile_image=_

Technically this is the third part of the project :P. I named it incorrectly, though you can access it using `*127.0.0.1:8000/second/?id=_&employee_name=_&employee_age=_&employee_salary=_&profile_image=_*`

This will add a new entry in the database.


### Third/id/?id=_&employee_name=_&employee_age=_&employee_salary=_&profile_image=_

Technically this is the fourth part of the project. You can access it using `*127.0.0.1:8000/second/id/?id=_&employee_name=_&employee_age=_&employee_salary=_&profile_image=_*`

This will edit an existing entry using the employee id, if correctly entered. You can edit the id itself using this.

### Fourth/?id=_

Technically this is the Fifth and final part.You can access it using `*127.0.0.1:8000/second/?id=_*`

This will delete the entry of the particular employee

### Fifth

This will delete the entire database. and next time you wull again have to go to `/first` again to re-aqqire the database.


##### Thanks.
